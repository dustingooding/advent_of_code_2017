#!/usr/bin/env python

import unittest

def read_input(filename):
    with open(filename) as f:
        input_data = f.readlines()
    return [x.strip().split('\t') for x in input_data]


def checksum_range(table):
    s = 0
    for row in table:
        biggest = -1
        smallest = 9999
        for col in row:
            if int(col) > biggest: biggest = int(col)
            if int(col) < smallest: smallest = int(col)
        s += (biggest - smallest)
    return s


def checksum_divisible(table):
    s = 0
    for row in table:
        for i in range(len(row)-1):
            for j in range(i+1,len(row)):
                if (int(row[i]) % int(row[j]) == 0) or (int(row[j]) % int(row[i]) == 0):
                    if int(row[i]) > int(row[j]):
                        s += int(int(row[i]) / int(row[j]))
                    else:
                        s += int(int(row[j]) / int(row[i]))
                    break
    return s


class ChecksumTest(unittest.TestCase):
    def test_range(self):
        self.assertEqual(checksum_range([['5','1','9','5'],['7','5','3'],['2','4','6','8']]), 18)

    def test_divisible(self):
        self.assertEqual(checksum_divisible([['5','9','2','8'],['9','4','7','3'],['3','8','6','5']]), 9)


if __name__ == '__main__':
    input_data = read_input('input')
    print('2-1:', checksum_range(input_data))
    print('2-2:', checksum_divisible(input_data))

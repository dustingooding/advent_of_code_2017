#!/usr/bin/env python

import unittest

def read_input(filename):
    with open(filename) as f:
        input_data = f.readlines()
    return [x.strip() for x in input_data]


def sum_captcha_next(captcha):
    s = 0
    for i in range(len(captcha)):
        if int(captcha[i]) == int(captcha[(i+1)%len(captcha)]):
            s += int(captcha[i])
    return s


def sum_captcha_halfway(captcha):
    s = 0
    for i in range(len(captcha)):
        if int(captcha[i]) == int(captcha[(i+int(len(captcha)/2))%len(captcha)]):
            s += int(captcha[i])
    return s


class CaptchaTest(unittest.TestCase):
    def test_next(self):
        self.assertEqual(sum_captcha_next('1122'), 3)
        self.assertEqual(sum_captcha_next('1111'), 4)
        self.assertEqual(sum_captcha_next('1234'), 0)
        self.assertEqual(sum_captcha_next('91212129'), 9)

    def test_halfway(self):
        self.assertEqual(sum_captcha_halfway('1212'), 6)
        self.assertEqual(sum_captcha_halfway('1221'), 0)
        self.assertEqual(sum_captcha_halfway('123425'), 4)
        self.assertEqual(sum_captcha_halfway('123123'), 12)
        self.assertEqual(sum_captcha_halfway('12131415'), 4)


if __name__ == '__main__':
    input_data_stripped = read_input('input')
    print('1-1:', sum_captcha_next(input_data_stripped[0]))
    print('1-2:', sum_captcha_halfway(input_data_stripped[0]))
